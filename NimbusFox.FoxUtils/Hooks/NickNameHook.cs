﻿using System;
using System.Collections.Generic;
using System.IO;
using NimbusFox.FoxUtilities.Classes.BlobFiles;
using NimbusFox.KitsuneCore.V1;
using Plukit.Base;
using Staxel.Logic;

namespace NimbusFox.FoxUtilities.Hooks {
    internal class NickNameHook : IDisposable {
        internal const string NickNameFileName = "nicknames.json";
        internal const string OriginalFileName = "originals.db";

        public void Dispose() {
            Save();
            _origBlob.Dispose();
            Blob.Deallocate(ref _nickNameBlob);
        }

        private Blob _nickNameBlob;
        private BasicStringBlobFile _origBlob;

        internal NickNameHook() {
            _nickNameBlob = BlobAllocator.Blob(true);

            _origBlob = new BasicStringBlobFile(
                UtilityHook.KSCore.ConfigDirectory.FetchDirectory("NickNames")
                    .ObtainFileStream(OriginalFileName, FileMode.OpenOrCreate), true);

            Load();
        }

        internal void OnPlayerConnect(Entity entity) {
            if (entity?.PlayerEntityLogic != null) {
                if (_nickNameBlob.Contains(entity.PlayerEntityLogic.Uid())) {
                    entity.PlayerEntityLogic.SetPrivateFieldValue("_username", _nickNameBlob.GetString(entity.PlayerEntityLogic.Uid()));
                }
            }
        }

        internal void Save() {
            UtilityHook.KSCore.ConfigDirectory.FetchDirectory("NickNames").WriteFile(NickNameFileName, _nickNameBlob.ToString());
        }

        internal void Load() {
            Blob.Deallocate(ref _nickNameBlob);
            _nickNameBlob = BlobAllocator.Blob(true);
            if (UtilityHook.KSCore.ConfigDirectory.FetchDirectory("NickNames").FileExists(NickNameFileName)) {
                try {
                    _nickNameBlob.ReadJson(UtilityHook.KSCore.ConfigDirectory.FetchDirectory("NickNames")
                        .ReadFile(NickNameFileName));
                } catch {
                    // ignore
                }
            }

            Save();
        }

        internal void SetNickName(Entity entity, string nickName) {
            if (entity?.PlayerEntityLogic != null) {
                if (!_origBlob.Contains(entity.PlayerEntityLogic.Uid())) {
                    _origBlob.SetString(entity.PlayerEntityLogic.Uid(), entity.PlayerEntityLogic.DisplayName());
                }

                _nickNameBlob.SetString(entity.PlayerEntityLogic.Uid(), nickName);

                entity.PlayerEntityLogic.SetPrivateFieldValue("_username", nickName);

                Save();
            }
        }

        internal void ResetNickName(Entity entity) {
            if (entity?.PlayerEntityLogic != null) {
                if (_origBlob.Contains(entity.PlayerEntityLogic.Uid())) {
                    entity.PlayerEntityLogic.SetPrivateFieldValue("_username", _origBlob.GetString(entity.PlayerEntityLogic.Uid()));
                }

                if (_nickNameBlob.Contains(entity.PlayerEntityLogic.Uid())) {
                    _nickNameBlob.Delete(entity.PlayerEntityLogic.Uid());
                }

                Save();
            }
        }

        internal void RemoveNickName(string name) {
            foreach (var blob in new Dictionary<string, BlobEntry>(_nickNameBlob.KeyValueIteratable)) {
                if (blob.Value.Kind == BlobEntryKind.String) {
                    var nick = blob.Value.GetString();
                    if (string.Equals(name, nick, StringComparison.CurrentCultureIgnoreCase)) {
                        _nickNameBlob.Delete(blob.Key);
                    }
                }
            }
        }

        internal string GetOrigName(Entity entity) {
            if (entity?.PlayerEntityLogic != null) {
                if (_origBlob.Contains(entity.PlayerEntityLogic.Uid())) {
                    return _origBlob.GetString(entity.PlayerEntityLogic.Uid());
                }
            }

            return null;
        }
    }
}
