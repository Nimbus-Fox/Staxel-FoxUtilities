﻿using System;
using System.Diagnostics;
using System.Linq;
using Plukit.Base;
using Staxel.Logic;

namespace NimbusFox.FoxUtilities.Hooks {
    internal class ReservedSpaceHook : IDisposable {
        public void Dispose() { }

        private int limit { get; }

        public ReservedSpaceHook() {
            var parser = new CommandLineParser(Process.GetCurrentProcess().StartInfo.Arguments.Split(' '));
            limit = -1;
            if (parser.Parameters.TryGetValue("playerLimit", out var value)) {
                if (int.TryParse(value, out var l)) {
                    limit = l;
                }
            }

            if (limit == -1) {
                limit = int.MaxValue;
            }
        }

        public bool OnPlayerConnect(Entity player) {
            var current = UtilityHook.KSCore.UserManager.GetPlayerEntities().Count(x => x != player);

            if (current + UtilityHook.Instance.ServerConfig.ReservedSpaceConfig.Reserved >= limit) {
                var connection = UtilityHook.KSCore.ServerMainLoop.FetchConnectionsByUid(player.PlayerEntityLogic.Uid());

                foreach (var con in connection) {
                    con.ErrorAndClose(UtilityHook.Instance.ServerConfig.ReservedSpaceConfig.Message);
                }

                return false;
            }

            return true;
        }
    }
}
