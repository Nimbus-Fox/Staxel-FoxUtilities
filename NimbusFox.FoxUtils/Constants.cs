﻿namespace NimbusFox.FoxUtilities {
    public static class Constants {
        public static readonly Classes.Version Version = new Classes.Version(0, 1);

        public static class Colors {
            public const string Command = "^c:F2E200;";
            public const string SubCommandSeparator = "^c:34EA80;|^c:pop;";
            public const string SubCommand = "^c:35EAE1;";
            public const string RequiredArgument = "^c:92F442;";
            public const string OptionalArgument = "^c:99929B;";
            public const string Error = "^c:FF0000;";
            public const string FoxUtils = "^c:D86608;FoxUtils^c:pop;";
            public const string Success = "^c:92F442;";
            public const string Info = "^c:34EA80;";
            public const string Petals = "^c:F79125;";
        }
    }
}
