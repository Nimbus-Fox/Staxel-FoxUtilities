﻿using NimbusFox.FoxUtils.Classes;
using NimbusFox.KitsuneCore.V1;
using Plukit.Base;

namespace NimbusFox.FoxUtilities.Classes.BlobFiles.ConfigBlobs {
    internal class BackupConfig : BaseConfig {
        public BackupConfig(ConfigBlobFile file, Blob blob) : base(file, blob) { }

        public bool Enabled {
            get => Blob.GetBool("_enabled", false);
            set {
                Blob.SetBool("_enabled", value);
                File.InternalSave();
            }
        }

        public byte Hour {
            get {
                return Blob.FetchByte("hour", 2);
            }
            set {
                Blob.SetLong("hour", value);
                File.InternalSave();
            }
        }

        public byte Minute {
            get {
                if (byte.TryParse(Blob.GetLong("minute", 30).ToString(), out var minute)) {
                    if (minute < 60) {
                        return minute;
                    }
                }

                return 30;
            }
            set {
                Blob.SetLong("minute", value);
                File.InternalSave();
            }
        }

        internal override void Defaults() {
            Enabled = Enabled;
            Hour = Hour;
            Minute = Minute;

            Blob.SetString("hour_comment", "How many hours and minutes between each backup");
            Blob.SetString("minute_comment", "How many hours and minutes between each backup");
        }
    }
}
