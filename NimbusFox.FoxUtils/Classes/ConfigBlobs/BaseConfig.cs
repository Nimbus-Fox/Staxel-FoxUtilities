﻿using NimbusFox.FoxUtils.Classes;
using Plukit.Base;

namespace NimbusFox.FoxUtilities.Classes.BlobFiles.ConfigBlobs {
    internal abstract class BaseConfig {
        protected Blob Blob;
        protected ConfigBlobFile File;

        public BaseConfig(ConfigBlobFile file, Blob blob) {
            File = file;
            Blob = blob;
        }

        internal abstract void Defaults();
    }
}
