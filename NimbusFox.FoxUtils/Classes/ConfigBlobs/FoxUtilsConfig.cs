﻿using NimbusFox.FoxUtils.Classes;
using Plukit.Base;

namespace NimbusFox.FoxUtilities.Classes.BlobFiles.ConfigBlobs {
    class FoxUtilsConfig : BaseConfig {
        public FoxUtilsConfig(ConfigBlobFile file, Blob blob) : base(file, blob) { }

        public bool Enabled {
            get => Blob.GetBool("_enabled", false);
            set {
                Blob.SetBool("_enabled", value);
                File.InternalSave();
            }
        }

        public bool AllowUserMail {
            get => Blob.GetBool("allowUserMail", false);
            set {
                Blob.SetBool("allowUserMail", value);
                File.InternalSave();
            }
        }

        internal override void Defaults() {
            Enabled = Enabled;

            Blob.SetString("_enabled_comment", "FoxUtils is disabled by default so not to disturb the single player experience");
        }
    }
}
