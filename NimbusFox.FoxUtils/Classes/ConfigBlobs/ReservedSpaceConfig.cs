﻿using System.Collections.Generic;
using System.Linq;
using NimbusFox.FoxUtils.Classes;
using NimbusFox.KitsuneCore.V1;
using Plukit.Base;

namespace NimbusFox.FoxUtilities.Classes.BlobFiles.ConfigBlobs {
    internal class ReservedSpaceConfig : BaseConfig {
        public ReservedSpaceConfig(ConfigBlobFile file, Blob blob) : base(file, blob) { }

        public bool Enabled {
            get => Blob.GetBool("_enabled", false);
            set {
                Blob.SetBool("_enabled", value);
                File.InternalSave();
            }
        }

        public long Reserved {
            get => Blob.GetLong("reserved", 3);
            set {
                Blob.SetLong("reserved", value);
                File.InternalSave();
            }
        }

        public List<string> Users {
            get {
                if (Blob.Contains("users")) {
                    return Blob.FetchList("users", new List<string>());
                }

                return new List<string>();
            }
            set {
                Blob.SetList("users", value);
                File.InternalSave();
            }
        }

        public string Message {
            get => Blob.GetString("message", $"Unable to join the server. There are {Reserved} reserved slots");
            set {
                Blob.SetString("message", value);
                File.InternalSave();
            }
        }

        internal override void Defaults() {
            Enabled = Enabled;
            Reserved = Reserved;
            Users = Users;
            Message = Message;
        }
    }
}
