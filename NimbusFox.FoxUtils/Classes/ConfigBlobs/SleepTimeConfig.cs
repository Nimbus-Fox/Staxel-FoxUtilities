﻿using NimbusFox.FoxUtils.Classes;
using Plukit.Base;
using Staxel.Sky;

namespace NimbusFox.FoxUtilities.Classes.BlobFiles.ConfigBlobs {
    internal class SleepTimeConfig : BaseConfig {
        public SleepTimeConfig(ConfigBlobFile file, Blob blob) : base(file, blob) {
            DayLength = new Seasons(Blob.FetchBlob("dayLength"), File);
        }

        public bool Enabled {
            get => Blob.GetBool("_enabled", true);
            set {
                Blob.SetBool("_enabled", value);
                File.InternalSave();
            }
        }

        public bool Broadcast {
            get => Blob.GetBool("_broadcast", true);
            set {
                Blob.SetBool("_broadcast", value);
                File.InternalSave();
            }
        }

        public byte PercentageToForceSleep {
            get {
                const byte def = 25;

                var value = Blob.GetLong("percentageToForceSleep", def);

                if (byte.TryParse(value.ToString(), out var byt)) {
                    if (byt <= 100) {
                        return byt;
                    }
                }

                return def;
            }
            set {
                if (byte.TryParse(value.ToString(), out var byt)) {
                    if (byt <= 100) {
                        Blob.SetLong("percentageToForceSleep", byt);
                        File.InternalSave();
                    }
                }
            }
        }

        internal class Seasons {
            private Blob _data;
            private ConfigBlobFile _file;
            public Seasons(Blob data, ConfigBlobFile file) {
                _data = data;
                _file = file;
            }

            public int Spring {
                get {
                    var length = 24;

                    if (int.TryParse(_data.GetLong("spring", 24).ToString(), out var spring)) {
                        length = spring;
                    }

                    return length;
                }
                set {
                    _data.SetLong("spring", value);
                    _file.InternalSave();
                }
            }

            public int Summer {
                get {
                    var length = 24;

                    if (int.TryParse(_data.GetLong("summer", 24).ToString(), out var spring)) {
                        length = spring;
                    }

                    return length;
                }
                set {
                    _data.SetLong("summer", value);
                    _file.InternalSave();
                }
            }

            public int Autumn {
                get {
                    var length = 24;

                    if (int.TryParse(_data.GetLong("autumn", 24).ToString(), out var spring)) {
                        length = spring;
                    }

                    return length;
                }
                set {
                    _data.SetLong("autumn", value);
                    _file.InternalSave();
                }
            }

            public int Winter {
                get {
                    var length = 24;

                    if (int.TryParse(_data.GetLong("winter", 24).ToString(), out var spring)) {
                        length = spring;
                    }

                    return length;
                }
                set {
                    _data.SetLong("winter", value);
                    _file.InternalSave();
                }
            }

        }

        public Seasons DayLength { get; }

        public float WakeUpPhase {
            get => (float) Blob.GetDouble("wakeUpPhase", Staxel.Core.Constants.DawnPhase);
            set {
                Blob.SetDouble("wakeUpPhase", value);
                File.InternalSave();
            }
        }

        public float SleepPhase {
            get => (float) Blob.GetDouble("sleepPhase", Staxel.Core.Constants.SundownPhase);
            set {
                Blob.SetDouble("sleepPhase", value);
                File.InternalSave();
            }
        }

        internal override void Defaults() {
            Enabled = Enabled;
            Broadcast = Broadcast;
            PercentageToForceSleep = PercentageToForceSleep;
            WakeUpPhase = WakeUpPhase;
            SleepPhase = SleepPhase;
            DayLength.Spring = DayLength.Spring;
            DayLength.Summer = DayLength.Summer;
            DayLength.Autumn = DayLength.Autumn;
            DayLength.Winter = DayLength.Winter;
        }
    }
}
