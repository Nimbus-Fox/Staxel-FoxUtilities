﻿using System;
using System.Collections.Generic;
using System.Linq;
using NimbusFox.FoxUtilities.Hooks;
using NimbusFox.KitsuneCore.V1;
using NimbusFox.KitsuneCore.V1.Classes.BlobRecord;
using NimbusFox.KitsuneCore.V1.Rev1.Classes;
using Plukit.Base;
using Staxel.Logic;

namespace NimbusFox.FoxUtilities.Classes.DatabaseRecords.TeamFarming {
    public class TeamRecord : BaseRecord {
        private BlobDatabase _database;

        public TeamRecord(BlobDatabase database, Blob blob, Guid id) : base(database, blob, id) {
            _database = database;

            Banned = new List<string>();
            Invited = new List<string>();
        }

        public string Name {
            get => _blob.GetString(nameof(Name), "");
            set {
                _blob.SetString(nameof(Name), value);
                Save();
            }
        }

        public string OwnerUid {
            get => _blob.GetString(nameof(OwnerUid), "");
            set {
                _blob.SetString(nameof(OwnerUid), value);
                Save();
            }
        }

        public int Petals {
            get => _blob.FetchInt(nameof(Petals), 0);
            set {
                _blob.SetLong(nameof(Petals), value);
                Save();
            }
        }

        public bool SharePetals {
            get => _blob.GetBool(nameof(SharePetals), false);
            set {
                _blob.SetBool(nameof(SharePetals), value);
                Save();
            }
        }

        public int PetalDiff {
            get => _blob.FetchInt(nameof(PetalDiff), 0);
            internal set {
                _blob.SetLong(nameof(PetalDiff), value);
                Save();
            }
        }

        public bool Disbanded {
            get => _blob.GetBool(nameof(Disbanded), false);
            set {
                _blob.SetBool(nameof(Disbanded), value);
                Save();
            }
        }

        public bool AdminInviteOnly {
            get => _blob.GetBool(nameof(AdminInviteOnly), true);
            set {
                _blob.SetBool(nameof(AdminInviteOnly), value);
                Save();
            }
        }

        public IReadOnlyList<string> Banned {
            get => _blob.FetchList(nameof(Banned), new List<string>()).ToList();
            private set {
                _blob.SetList(nameof(Banned), value.ToList());
                Save();
            }
        }

        public IReadOnlyList<string> Invited {
            get => _blob.FetchList(nameof(Invited), new List<string>()).ToList();
            private set {
                _blob.SetList(nameof(Invited), value.ToList());
                Save();
            }
        }

        public void Ban(Entity player) {
            if (player?.PlayerEntityLogic != null) {
                Ban(player.PlayerEntityLogic.Uid());
            }
        }

        public void Ban(string uid) {
            var banned = new List<string>(Banned);
            if (!banned.Contains(uid)) {
                banned.Add(uid);
                Banned = banned;

                Kick(uid);
            }
        }

        public void UnBan(Entity player) {
            if (player?.PlayerEntityLogic != null) {
                UnBan(player.PlayerEntityLogic.Uid());
            }
        }

        public void UnBan(string uid) {
            var banned = new List<string>(Banned);
            if (banned.Contains(uid)) {
                banned.Remove(uid);
                Banned = banned;
            }
        }

        public void Invite(Entity player) {
            if (player?.PlayerEntityLogic != null) {
                Invite(player.PlayerEntityLogic.Uid());
            }
        }

        public void Invite(string uid) {
            var invited = new List<string>(Invited);
            if (!invited.Contains(uid)) {
                invited.Add(uid);
                Invited = invited;
            }
        }

        public void UnInvite(Entity player) {
            if (player?.PlayerEntityLogic != null) {
                UnInvite(player.PlayerEntityLogic.Uid());
            }
        }

        public void UnInvite(string uid) {
            var invited = new List<string>(Invited);
            if (invited.Contains(uid)) {
                invited.Remove(uid);
                Invited = invited;
            }
        }

        public void Kick(Entity player) {
            if (player?.PlayerEntityLogic != null) {
                Kick(player.PlayerEntityLogic.Uid());
            }
        }

        public void Kick(string uid) {
            var member = GetMembers().FirstOrDefault(x => x.MemberUid == uid);

            if (member != default(MemberRecord)) {
                if (member.TeamUid == ID) {
                    if (UtilityHook.KSCore.UserManager.IsUserOnline(uid)) {
                        var player = UtilityHook.KSCore.UserManager.GetPlayerEntityByUid(uid);
                        player.Inventory.ResetMoney((int)GetTenth());
                    } else {
                        member.KickedAmount += GetTenth();
                    }
                    member.TeamUid = Guid.Empty;
                    PetalDiff -= GetTenth();
                }
            }
        }

        public void Disband() {
            var dist = GetDisbandMoney();

            var members = GetMembers();

            foreach (var member in members) {
                member.TeamUid = Guid.Empty;
                if (UtilityHook.KSCore.UserManager.IsUserOnline(member.MemberUid)) {
                    var player = UtilityHook.KSCore.UserManager.GetPlayerEntityByUid(member.MemberUid);
                    player.Inventory.ResetMoney((int)dist);
                } else {
                    member.KickedAmount += dist;
                }
            }

            Disbanded = true;

            _database.RemoveRecord(ID);
        }

        public void Join(Entity entity) {
            if (entity?.PlayerEntityLogic != null) {
                if (Invited.Contains(entity.PlayerEntityLogic.Uid()) || OwnerUid == entity.PlayerEntityLogic.Uid()) {
                    var record = _database
                        .SearchRecords<MemberRecord>(x => x.MemberUid == entity.PlayerEntityLogic.Uid())
                        .FirstOrDefault();

                    if (record == default(MemberRecord)) {
                        record = _database.CreateRecord<MemberRecord>();
                        record.MemberUid = entity.PlayerEntityLogic.Uid();
                    }

                    record.TeamUid = ID;
                    record.TeamPetalDiff = 0;

                    if (SharePetals) {
                        entity.Inventory.ResetMoney(entity.Inventory.GetMoney() + Petals);
                    }

                    UnInvite(record.MemberUid);
                }
            }
        }

        public void Leave(Entity entity) {
            if (entity?.PlayerEntityLogic != null) {
                if (OwnerUid != entity.PlayerEntityLogic.Uid()) {
                    var record = _database
                        .SearchRecords<MemberRecord>(x => x.MemberUid == entity.PlayerEntityLogic.Uid())
                        .FirstOrDefault();

                    if (record == default(MemberRecord)) {
                        record = _database.CreateRecord<MemberRecord>();
                        record.MemberUid = entity.PlayerEntityLogic.Uid();
                    }

                    entity.Inventory.ResetMoney(GetTenth());

                    record.TeamUid = Guid.Empty;
                    record.TeamPetalDiff = 0;

                    PetalDiff -= GetTenth();
                }
            }
        }

        public List<MemberRecord> GetMembers() {
            return _database.SearchRecords<MemberRecord>(x => x.TeamUid == ID);
        }

        public int GetTenth() {
            if (GetMemberCount() >= 10) {
                return GetDisbandMoney();
            }
            return (int)Math.Floor((decimal)(Petals / 10));
        }

        public int GetDisbandMoney() {
            return (int)Math.Floor((decimal)(Petals / GetMemberCount()));
        }

        public long GetMemberCount() {
            return GetMembers().Count;
        }
    }
}
