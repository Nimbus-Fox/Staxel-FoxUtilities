﻿using System.IO;
using NimbusFox.KitsuneCore.V1.Classes;

namespace NimbusFox.FoxUtilities.Classes.BlobFiles {
    internal class BasicStringBlobFile : BlobFile {
        public BasicStringBlobFile(FileStream stream, bool binary = false) : base(stream, binary) { }

        public void SetString(string key, string value) {
            Blob.SetString(key, value);
            Save();
        }

        public void Delete(string key) {
            if (Blob.Contains(key)) {
                Blob.Delete(key);
                Save();
            }
        }

        public string GetString(string key, string _default) {
            return Blob.GetString(key, _default);
        }

        public string GetString(string key) {
            return Blob.GetString(key);
        }

        public bool Contains(string key) {
            return Blob.Contains(key);
        }
    }
}
