﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.FoxUtilities.Classes.BlobFiles.ConfigBlobs;
using NimbusFox.KitsuneCore.V1.Classes;
using Plukit.Base;

namespace NimbusFox.FoxUtils.Classes {
    internal class ConfigBlobFile : BlobFile {

        public NicknamesConfig NickNamesConfig { get; }
        public TeamFarmingConfig TeamFarmingConfig { get; }
        public SleepTimeConfig SleepTimeConfig { get; }
        public ReservedSpaceConfig ReservedSpaceConfig { get; }
        public BackupConfig BackupConfig { get; }
        public FoxUtilsConfig FoxUtilsConfig { get; }

        public ConfigBlobFile(FileStream stream, bool binary = false) : base(stream, binary) {
            NickNamesConfig = new NicknamesConfig(this, Blob.FetchBlob("nicknamesConfig"));
            TeamFarmingConfig = new TeamFarmingConfig(this, Blob.FetchBlob("teamFarmingConfig"));
            SleepTimeConfig = new SleepTimeConfig(this, Blob.FetchBlob("sleepTimeConfig"));
            ReservedSpaceConfig = new ReservedSpaceConfig(this, Blob.FetchBlob("reservedSpaceConfig"));
            BackupConfig = new BackupConfig(this, Blob.FetchBlob("backupConfig"));
            FoxUtilsConfig = new FoxUtilsConfig(this, Blob.FetchBlob("_foxUtilsConfig"));

            Defaults();
        }

        public ConfigBlobFile(Blob blob) : base(null) {
            Blob.MergeFrom(blob);
            NickNamesConfig = new NicknamesConfig(this, Blob.FetchBlob("nicknamesConfig"));
            TeamFarmingConfig = new TeamFarmingConfig(this, Blob.FetchBlob("teamFarmingConfig"));
            SleepTimeConfig = new SleepTimeConfig(this, Blob.FetchBlob("sleepTimeConfig"));
            ReservedSpaceConfig = new ReservedSpaceConfig(this, Blob.FetchBlob("reservedSpaceConfig"));
            BackupConfig = new BackupConfig(this, Blob.FetchBlob("backupConfig"));
            FoxUtilsConfig = new FoxUtilsConfig(this, Blob.FetchBlob("_foxUtilsConfig"));
        }

        internal void InternalSave() => Save();

        protected void Defaults() {
            NickNamesConfig.Defaults();
            TeamFarmingConfig.Defaults();
            SleepTimeConfig.Defaults();
            ReservedSpaceConfig.Defaults();
            BackupConfig.Defaults();
            FoxUtilsConfig.Defaults();

            ForceSave();
        }
    }
}
