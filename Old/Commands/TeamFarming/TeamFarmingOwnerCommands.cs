﻿using System;
using System.Globalization;
using System.Linq;
using NimbusFox.FoxUtilities.Classes.DatabaseRecords.TeamFarming;
using NimbusFox.FoxUtilities.Hooks;
using Plukit.Base;
using Staxel.Commands;
using Staxel.Logic;
using Staxel.Server;

namespace NimbusFox.FoxUtilities.Commands.TeamFarming {
    public class TeamFarmingOwnerCommands : ICommandBuilder {
        public string Execute(string[] bits, Blob blob, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            responseParams = new object[] { };

            var callingEntity = UtilityHook.FoxCore.UserManager.GetPlayerEntityByUid(connection.Credentials.Uid);

            if (!UtilityHook.Instance.ServerConfig.FoxUtilsConfig.Enabled) {
                return "nimbusfox.foxutils.message.disabled";
            }

            if (!UtilityHook.Instance.ServerConfig.TeamFarmingConfig.Enabled) {
                return "nimbusfox.foxutils.teamfarming.error.disabled";
            }

            if (!UtilityHook.Instance.TeamFarmingHook.IsUserInTeam(callingEntity, out var member)) {
                return "nimbusfox.foxutils.teamfarming.error.notinteam";
            }

            if (member.GetTeamRecord().OwnerUid != member.MemberUid) {
                return "nimbusfox.foxutils.teamfarming.error.notowner";
            }

            if (bits.Length > 1) {
                switch (bits[1].ToLower()) {
                    default:
                        return Help(connection, out responseParams);
                    case "rename":
                        return Rename(bits.Skip(2).ToArray(), callingEntity, member, connection, api,
                            out responseParams);
                    case "give":
                        return Give(bits.Skip(2).ToArray(), callingEntity, member, connection, api, out responseParams);
                    case "disband":
                        return Disband(bits.Skip(2).ToArray(), callingEntity, member, connection, api,
                            out responseParams);
                    case "invite":
                        return Invite(bits.Skip(2).ToArray(), callingEntity, member, connection, api,
                            out responseParams);
                    case "uninvite":
                        return UnInvite(bits.Skip(2).ToArray(), callingEntity, member, connection, api,
                            out responseParams);
                    case "kick":
                        return Kick(bits.Skip(2).ToArray(), callingEntity, member, connection, api, out responseParams);
                    case "ban":
                        return Ban(bits.Skip(2).ToArray(), callingEntity, member, connection, api, out responseParams);
                    case "unban":
                        return UnBan(bits.Skip(2).ToArray(), callingEntity, member, connection, api,
                            out responseParams);
                    case "toggleownerinvite":
                        return ToggleOwnerInvite(bits.Skip(2).ToArray(), callingEntity, member, connection, api,
                            out responseParams);
                    case "togglepetalshare":
                        return TogglePetalShare(bits.Skip(2).ToArray(), callingEntity, member, connection, api,
                            out responseParams);
                }
            }

            return Help(connection, out responseParams);
        }

        public string Kind => "tfowner";
        public string Usage => "nimbusfox.foxutils.teamfarming.command.tfowner";
        public bool Public => UtilityHook.Instance.ServerConfig.TeamFarmingConfig.Enabled &&
                              UtilityHook.Instance.ServerConfig.FoxUtilsConfig.Enabled;

        private string Help(ClientServerConnection connection, out object[] responseParams) {
            responseParams = new object[] { };

            UtilityHook.FoxCore.MessagePlayerByUid(connection.Credentials.Uid, "nimbusfox.foxutils.teamfarming.command.tfowner.help");
            UtilityHook.FoxCore.MessagePlayerByUid(connection.Credentials.Uid, "nimbusfox.foxutils.command.args");
            UtilityHook.FoxCore.MessagePlayerByUid(connection.Credentials.Uid, "nimbusfox.foxutils.teamfarming.command.tfowner.help.rename");
            UtilityHook.FoxCore.MessagePlayerByUid(connection.Credentials.Uid, "nimbusfox.foxutils.teamfarming.command.tfowner.help.give");
            UtilityHook.FoxCore.MessagePlayerByUid(connection.Credentials.Uid, "nimbusfox.foxutils.teamfarming.command.tfowner.help.disband");
            UtilityHook.FoxCore.MessagePlayerByUid(connection.Credentials.Uid, "nimbusfox.foxutils.teamfarming.command.tfowner.help.invite");
            UtilityHook.FoxCore.MessagePlayerByUid(connection.Credentials.Uid, "nimbusfox.foxutils.teamfarming.command.tfowner.help.uninvite");
            UtilityHook.FoxCore.MessagePlayerByUid(connection.Credentials.Uid, "nimbusfox.foxutils.teamfarming.command.tfowner.help.kick");
            UtilityHook.FoxCore.MessagePlayerByUid(connection.Credentials.Uid, "nimbusfox.foxutils.teamfarming.command.tfowner.help.ban");
            UtilityHook.FoxCore.MessagePlayerByUid(connection.Credentials.Uid, "nimbusfox.foxutils.teamfarming.command.tfowner.help.unban");
            UtilityHook.FoxCore.MessagePlayerByUid(connection.Credentials.Uid, "nimbusfox.foxutils.teamfarming.command.tfowner.help.toggleownerinvite");
            UtilityHook.FoxCore.MessagePlayerByUid(connection.Credentials.Uid, "nimbusfox.foxutils.teamfarming.command.tfowner.help.togglepetalshare");

            return "";
        }

        private static string Rename(string[] bits, Entity callingEntity, MemberRecord member, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            responseParams = new object[] { };

            if (!bits.Any()) {
                UtilityHook.FoxCore.MessagePlayerByUid(connection.Credentials.Uid, "nimbusfox.foxutils.command.args");
                return "nimbusfox.foxutils.teamfarming.command.tfowner.help.rename";
            }

            var team = member.GetTeamRecord();

            team.Name = bits[0];

            foreach (var tMember in team.GetMembers()) {
                UtilityHook.FoxCore.MessagePlayerByUid(tMember.MemberUid, "nimbusfox.foxutils.teamfarming.success.namechange", member.DisplayName, team.Name);
            }

            return "";
        }

        private static string Give(string[] bits, Entity callingEntity, MemberRecord member, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            responseParams = new object[] { };

            if (!bits.Any()) {
                UtilityHook.FoxCore.MessagePlayerByUid(connection.Credentials.Uid, "nimbusfox.foxutils.command.args");
                return "nimbusfox.foxutils.teamfarming.command.tfowner.help.give";
            }

            var uid = UtilityHook.FoxCore.UserManager.GetUidByName(bits[0]);

            if (uid == null) {
                responseParams = new object[] { bits[0] };
                return "nimbusfox.foxutils.teamfarming.error.notexists";
            }

            var team = member.GetTeamRecord();

            foreach (var tMember in team.GetMembers()) {
                if (tMember.MemberUid == uid) {
                    team.OwnerUid = tMember.MemberUid;
                    foreach (var ntMember in team.GetMembers()) {
                        UtilityHook.FoxCore.MessagePlayerByUid(ntMember.MemberUid, "nimbusfox.foxutils.teamfarming.success.newowner", tMember.DisplayName);
                    }

                    return "";
                }
            }

            responseParams = new object[] { bits[0] };
            return "nimbusfox.foxutils.teamfarming.error.tfowner.notinteam";
        }

        private static string Disband(string[] bits, Entity callingEntity, MemberRecord member, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            responseParams = new object[] { };

            if (!bits.Any()) {
                UtilityHook.FoxCore.MessagePlayerByUid(connection.Credentials.Uid, "nimbusfox.foxutils.command.args");
                return "nimbusfox.foxutils.teamfarming.command.tfowner.help.disband";
            }

            var team = member.GetTeamRecord();

            if (bits[0] == team.Name) {
                var members = team.GetMembers();
                var dist = team.GetDisbandMoney();
                var name = team.Name;
                team.Disband();
                foreach (var mem in members) {
                    UtilityHook.FoxCore.MessagePlayerByUid(mem.MemberUid, "nimbusfox.foxutils.teamfarming.message.teamdisanded", name, dist.ToString("N0"));
                }

                return "";
            }

            return "nimbusfox.foxutils.teamfarming.error.namenomatch";
        }

        private static string Invite(string[] bits, Entity callingEntity, MemberRecord callingMember, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            responseParams = new object[] { };

            if (!bits.Any()) {
                UtilityHook.FoxCore.MessagePlayerByUid(connection.Credentials.Uid, "nimbusfox.foxutils.command.args");
                return "nimbusfox.foxutils.teamfarming.command.tfowner.help.invite";
            }

            var name = string.Join(" ", bits);
            var uid = UtilityHook.FoxCore.UserManager.GetUidByName(name);

            if (uid == null) {
                responseParams = new object[] { name };
                return "nimbusfox.foxutils.teamfarming.error.notonserver";
            }

            if (UtilityHook.Instance.TeamFarmingHook.IsUserInTeam(name, out var member)) {
                responseParams = new object[] { name };
                return "nimbusfox.foxutils.teamfarming.error.currentlyinteam";
            }

            var team = callingMember.GetTeamRecord();

            if (team.Banned.Contains(uid)) {
                responseParams = new object[] { name };
                return "nimbusfox.foxutils.teamfarming.error.inviteban";
            }

            if (team.GetMembers()
                .Any(x => string.Equals(x.DisplayName, name, StringComparison.CurrentCultureIgnoreCase))) {
                responseParams = new object[] { name };
                return "nimbusfox.foxutils.teamfarming.error.inviteinteam";
            }

            team.Invite(uid);

            foreach (var mem in team.GetMembers()) {
                UtilityHook.FoxCore.MessagePlayerByUid(mem.MemberUid, "nimbusfox.foxutils.teamfarming.success.newinvite", callingMember.DisplayName, name);
            }

            UtilityHook.FoxCore.MessagePlayerByUid(uid, "nimbusfox.foxutils.teamfarming.message.invitetoteam", team.Name, callingMember.DisplayName);

            return "";
        }

        private static string UnInvite(string[] bits, Entity callingEntity, MemberRecord callingMember, ClientServerConnection connection,
            ICommandsApi api, out object[] responseParams) {
            responseParams = new object[] { };

            if (!bits.Any()) {
                UtilityHook.FoxCore.MessagePlayerByUid(connection.Credentials.Uid, "nimbusfox.foxutils.command.args");
                return "nimbusfox.foxutils.teamfarming.command.tfowner.help.uninvite";
            }

            var name = string.Join(" ", bits);
            var uid = UtilityHook.FoxCore.UserManager.GetUidByName(name);

            var team = callingMember.GetTeamRecord();

            if (!team.Invited.Contains(uid)) {
                responseParams = new object[] { name };
                return "nimbusfox.foxutils.teamfarming.error.notinvited";
            }

            team.UnInvite(uid);

            foreach (var mem in team.GetMembers()) {
                UtilityHook.FoxCore.MessagePlayerByUid(mem.MemberUid, "nimbusfox.foxutils.teamfarming.success.uninvite", callingMember.DisplayName, name);
            }

            UtilityHook.FoxCore.MessagePlayerByUid(uid, "nimbusfox.foxutils.teamfarming.message.nolongerinvited", team.Name, callingMember.DisplayName);

            return "";
        }

        private static string Kick(string[] bits, Entity callingEntity, MemberRecord callingMember, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            responseParams = new object[] { };

            if (!bits.Any()) {
                UtilityHook.FoxCore.MessagePlayerByUid(connection.Credentials.Uid, "nimbusfox.foxutils.command.args");
                return "nimbusfox.foxutils.teamfarming.command.tfowner.help.kick";
            }

            var name = string.Join(" ", bits);
            var team = callingMember.GetTeamRecord();

            if (!team.GetMembers()
                .Any(x => string.Equals(x.DisplayName, name, StringComparison.CurrentCultureIgnoreCase))) {
                responseParams = new object[] { name };
                return "nimbusfox.foxutils.teamfarming.error.tfowner.notinteam";
            }

            var member = team.GetMembers().First(x =>
                string.Equals(x.DisplayName, name.ToLower(), StringComparison.CurrentCultureIgnoreCase));

            team.Kick(member.MemberUid);

            foreach (var mem in team.GetMembers()) {
                UtilityHook.FoxCore.MessagePlayerByUid(mem.MemberUid, "nimbusfox.foxutils.teamfarming.success.kicked",
                    callingMember.DisplayName, member.DisplayName, member.KickedAmount.ToString("N0"));
            }

            UtilityHook.FoxCore.MessagePlayerByUid(member.MemberUid, "nimbusfox.foxutils.teamfarming.message.kicked",
                team.Name, member.KickedAmount.ToString("N0"));

            return "";
        }

        private static string Ban(string[] bits, Entity callingEntity, MemberRecord callingMember, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            responseParams = new object[] { };

            if (!bits.Any()) {
                UtilityHook.FoxCore.MessagePlayerByUid(connection.Credentials.Uid, "nimbusfox.foxutils.command.args");
                return "nimbusfox.foxutils.teamfarming.command.tfowner.help.ban";
            }

            var name = string.Join(" ", bits);
            var uid = UtilityHook.FoxCore.UserManager.GetUidByName(name);

            if (uid == null) {
                responseParams = new object[] { name };
                return "nimbusfox.foxutils.teamfarming.error.notonserver";
            }

            var team = callingMember.GetTeamRecord();

            if (team.Banned.Contains(uid)) {
                responseParams = new object[] { name };
                return "nimbusfox.foxutils.teamfarming.error.alreadybanned";
            }

            var member = team.GetMembers().FirstOrDefault(x =>
                string.Equals(x.DisplayName, name.ToLower(), StringComparison.CurrentCultureIgnoreCase));

            team.Ban(uid);

            if (member != default(MemberRecord)) {
                foreach (var mem in team.GetMembers()) {
                    UtilityHook.FoxCore.MessagePlayerByUid(mem.MemberUid, "nimbusfox.foxutils.teamfarming.success.kicked",
                        callingMember.DisplayName, member.DisplayName, member.KickedAmount.ToString("N0"));
                }

                UtilityHook.FoxCore.MessagePlayerByUid(member.MemberUid, "nimbusfox.foxutils.teamfarming.message.kicked",
                    team.Name, member.KickedAmount.ToString("N0"));
            }

            foreach (var mem in team.GetMembers()) {
                UtilityHook.FoxCore.MessagePlayerByUid(mem.MemberUid, "nimbusfox.foxutils.teamfarming.success.banned",
                    name, callingMember.DisplayName);
            }

            UtilityHook.FoxCore.MessagePlayerByName(name, "nimbusfox.foxutils.teamfarming.message.banned", team.Name,
                callingMember.DisplayName);

            return "";
        }

        private static string UnBan(string[] bits, Entity callingEntity, MemberRecord callingMember, ClientServerConnection connection,
            ICommandsApi api, out object[] responseParams) {
            responseParams = new object[] { };

            if (!bits.Any()) {
                UtilityHook.FoxCore.MessagePlayerByUid(connection.Credentials.Uid, "nimbusfox.foxutils.command.args");
                return "nimbusfox.foxutils.teamfarming.command.tfowner.help.unban";
            }

            var name = string.Join(" ", bits);
            var uid = UtilityHook.FoxCore.UserManager.GetUidByName(name);

            if (uid == null) {
                responseParams = new object[] { name };
                return "nimbusfox.foxutils.teamfarming.error.notonserver";
            }

            var team = callingMember.GetTeamRecord();

            if (!team.Banned.Contains(uid)) {
                responseParams = new object[] { name };
                return "nimbusfox.foxutils.teamfarming.error.notbanned";
            }

            foreach (var mem in team.GetMembers()) {
                UtilityHook.FoxCore.MessagePlayerByUid(mem.MemberUid, "nimbusfox.foxutils.teamfarming.success.unbanned",
                    name, callingMember.DisplayName);
            }

            UtilityHook.FoxCore.MessagePlayerByName(name, "nimbusfox.foxutils.teamfarming.message.unbanned", team.Name,
                callingMember.DisplayName);

            return "";
        }

        private static string ToggleOwnerInvite(string[] bits, Entity callingEntity, MemberRecord callingMember,
            ClientServerConnection connection, ICommandsApi api, out object[] responseParams) {
            responseParams = new object[] { };

            var team = callingMember.GetTeamRecord();

            team.AdminInviteOnly = !team.AdminInviteOnly;

            foreach (var mem in team.GetMembers()) {
                UtilityHook.FoxCore.MessagePlayerByUid(mem.MemberUid,
                    "nimbusfox.foxutils.teamfarming.success.ownerinvitetoggle", callingMember.DisplayName,
                    CultureInfo.CurrentCulture.TextInfo.ToTitleCase(team.AdminInviteOnly.ToString()));
            }

            return "";
        }

        private static string TogglePetalShare(string[] bits, Entity callingEntity, MemberRecord callingMember,
            ClientServerConnection connection, ICommandsApi api, out object[] responseParams) {
            responseParams = new object[] { };

            var team = callingMember.GetTeamRecord();

            team.SharePetals = !team.SharePetals;

            foreach (var mem in team.GetMembers()) {
                if (team.SharePetals) {
                    if (UtilityHook.FoxCore.UserManager.IsUserOnline(mem.MemberUid)) {
                        var player = UtilityHook.FoxCore.UserManager.GetPlayerEntityByUid(mem.MemberUid);
                        team.Petals += player.Inventory.GetMoney();
                    } else {
                        mem.Imported = false;
                    }
                } else {
                    if (UtilityHook.FoxCore.UserManager.IsUserOnline(mem.MemberUid)) {
                        var player = UtilityHook.FoxCore.UserManager.GetPlayerEntityByUid(mem.MemberUid);
                        player.Inventory.ResetMoney(team.GetDisbandMoney());
                    } else {
                        mem.KickedAmount = team.GetDisbandMoney();
                    }
                }
            }

            if (team.SharePetals) {
                foreach (var mem in team.GetMembers()) {
                    if (UtilityHook.FoxCore.UserManager.IsUserOnline(mem.MemberUid)) {
                        var player = UtilityHook.FoxCore.UserManager.GetPlayerEntityByUid(mem.MemberUid);
                        player.Inventory.ResetMoney(team.Petals);
                    }
                }
            } else {
                team.Petals = 0;
            }

            foreach (var mem in team.GetMembers()) {
                UtilityHook.FoxCore.MessagePlayerByUid(mem.MemberUid,
                    "nimbusfox.foxutils.teamfarming.success.petalsharetoggle", callingMember.DisplayName,
                    CultureInfo.CurrentCulture.TextInfo.ToTitleCase(team.SharePetals.ToString()));
            }

            return "";
        }
    }
}
