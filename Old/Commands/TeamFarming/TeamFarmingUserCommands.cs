﻿using System;
using System.Linq;
using NimbusFox.FoxUtilities.Hooks;
using Plukit.Base;
using Staxel.Commands;
using Staxel.Logic;
using Staxel.Server;

namespace NimbusFox.FoxUtilities.Commands.TeamFarming {
    public class TeamFarmingUserCommands : ICommandBuilder {
        public string Execute(string[] bits, Blob blob, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            responseParams = new object[] { };

            var callingEntity = UtilityHook.FoxCore.UserManager.GetPlayerEntityByUid(connection.Credentials.Uid);

            if (!UtilityHook.Instance.ServerConfig.FoxUtilsConfig.Enabled) {
                return "nimbusfox.foxutils.message.disabled";
            }

            if (!UtilityHook.Instance.ServerConfig.TeamFarmingConfig.Enabled) {
                return "nimbusfox.foxutils.teamfarming.error.disabled";
            }

            if (bits.Length > 1) {
                switch (bits[1].ToLower()) {
                    default:
                        return Help(bits.Skip(2).ToArray(), connection, api, out responseParams);
                    case "create":
                        return Create(bits.Skip(2).ToArray(), connection, api, out responseParams);
                    case "join":
                        return Join(bits.Skip(2).ToArray(), callingEntity, connection, api, out responseParams);
                    case "leave":
                        return Leave(bits.Skip(2).ToArray(), callingEntity, connection, api, out responseParams);
                    case "status":
                        return Status(bits.Skip(2).ToArray(), callingEntity, connection, api, out responseParams);
                    case "invite":
                        return Invite(bits.Skip(2).ToArray(), callingEntity, connection, api, out responseParams);
                }
            }

            return Help(bits, connection, api, out responseParams);
        }

        public string Kind => "tf";
        public string Usage => "nimbusfox.foxutils.teamfarming.command.tf";
        public bool Public => UtilityHook.Instance.ServerConfig.TeamFarmingConfig.Enabled &&
                              UtilityHook.Instance.ServerConfig.FoxUtilsConfig.Enabled;

        private static string Help(string[] bits, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            responseParams = new object[] { };

            UtilityHook.FoxCore.MessagePlayerByUid(connection.Credentials.Uid, "nimbusfox.foxutils.teamfarming.command.tf.help");
            UtilityHook.FoxCore.MessagePlayerByUid(connection.Credentials.Uid, "nimbusfox.foxutils.command.args");
            UtilityHook.FoxCore.MessagePlayerByUid(connection.Credentials.Uid, "nimbusfox.foxutils.teamfarming.command.tf.help.create");
            UtilityHook.FoxCore.MessagePlayerByUid(connection.Credentials.Uid, "nimbusfox.foxutils.teamfarming.command.tf.help.join");
            UtilityHook.FoxCore.MessagePlayerByUid(connection.Credentials.Uid, "nimbusfox.foxutils.teamfarming.command.tf.help.leave");
            UtilityHook.FoxCore.MessagePlayerByUid(connection.Credentials.Uid, "nimbusfox.foxutils.teamfarming.command.tf.help.status");
            UtilityHook.FoxCore.MessagePlayerByUid(connection.Credentials.Uid, "nimbusfox.foxutils.teamfarming.command.tfowner.help.invite");

            return "";
        }

        private static string Create(string[] bits, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            responseParams = new object[] { };

            var playerEntity = UtilityHook.FoxCore.UserManager.GetPlayerEntityByUid(connection.Credentials.Uid);

            if (UtilityHook.Instance.TeamFarmingHook.IsUserInTeam(playerEntity, out _)) {
                return "nimbusfox.foxutils.teamfarming.error.userinteam";
            }

            if (!bits.Any()) {
                UtilityHook.FoxCore.MessagePlayerByUid(connection.Credentials.Uid, "nimbusfox.foxutils.command.args");
                UtilityHook.FoxCore.MessagePlayerByUid(connection.Credentials.Uid, "nimbusfox.foxutils.teamfarming.command.tf.help.create");
                return "";
            }

            var adminOnly = true;
            var sharePetals = false;

            var name = bits[0];

            if (bits.Length > 1) {
                if (bool.TryParse(bits[1], out var petals)) {
                    sharePetals = petals;
                }
            }

            if (bits.Length > 2) {
                if (bool.TryParse(bits[2], out var only)) {
                    adminOnly = only;
                }
            }

            if (UtilityHook.Instance.TeamFarmingHook.TeamExists(name)) {
                responseParams = new object[] { bits[0] };
                return "nimbusfox.foxutils.teamfarming.error.teamexistswithname";
            }

            var team = UtilityHook.Instance.TeamFarmingHook.CreateTeam(name, sharePetals, adminOnly);

            team.OwnerUid = connection.Credentials.Uid;

            team.Join(playerEntity);

            responseParams = new object[] { team.Name };

            return "nimbusfox.foxutils.teamfarming.success.createteam";
        }

        private static string Join(string[] bits, Entity callingEntity, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            responseParams = new object[] { };

            if (UtilityHook.Instance.TeamFarmingHook.IsUserInTeam(callingEntity, out _)) {
                return "nimbusfox.foxutils.teamfarming.error.userinteam";
            }

            if (!bits.Any()) {
                UtilityHook.FoxCore.MessagePlayerByUid(connection.Credentials.Uid, "nimbusfox.foxutils.command.args");
                return "nimbusfox.foxutils.teamfarming.command.tf.help.join";
            }

            var name = string.Join(" ", bits);

            if (!UtilityHook.Instance.TeamFarmingHook.TeamExists(name)) {
                responseParams = new object[] { name };
                return "nimbusfox.foxutils.teamfarming.error.noteamexists";
            }

            var team = UtilityHook.Instance.TeamFarmingHook.GetTeam(name);

            if (!team.Invited.Contains(callingEntity.PlayerEntityLogic.Uid())) {
                responseParams = new object[] { team.Name };
                return "nimbusfox.foxutils.teamfarming.error.notinvited";
            }

            team.Join(callingEntity);

            foreach (var mem in team.GetMembers()) {
                UtilityHook.FoxCore.MessagePlayerByUid(mem.MemberUid, "nimbusfox.foxutils.teamfarming.success.joined",
                    callingEntity.PlayerEntityLogic.DisplayName(), team.Name);
            }

            return "";
        }

        private static string Leave(string[] bits, Entity callingEntity, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            responseParams = new object[] { };

            if (!UtilityHook.Instance.TeamFarmingHook.IsUserInTeam(callingEntity, out var member)) {
                return "nimbusfox.foxutils.teamfarming.error.notinteam";
            }

            var team = member.GetTeamRecord();

            if (member.MemberUid == team.OwnerUid) {
                return "nimbusfox.foxutils.teamfarming.error.ownerofteam";
            }

            var petals = team.GetTenth();

            foreach (var mem in team.GetMembers()) {
                UtilityHook.FoxCore.MessagePlayerByUid(mem.MemberUid, "nimbusfox.foxutils.teamfarming.success.leave", member.DisplayName, petals.ToString("N0"));
            }

            team.Leave(callingEntity);

            return "";
        }

        private static string Status(string[] bits, Entity callingEntity, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            responseParams = new object[] { };

            if (!UtilityHook.Instance.TeamFarmingHook.IsUserInTeam(callingEntity, out var member)) {
                return "nimbusfox.foxutils.teamfarming.error.notinteam";
            }

            var team = member.GetTeamRecord();

            string petals;

            if (team.SharePetals) {
                petals = team.Petals.ToString("N0");
            } else {
                var pet = 0;
                foreach (var mem in team.GetMembers()) {
                    if (UtilityHook.FoxCore.UserManager.IsUserOnline(mem.MemberUid)) {
                        pet += UtilityHook.FoxCore.UserManager.GetPlayerEntityByUid(mem.MemberUid).Inventory
                            .GetMoney();
                    }
                }

                petals = pet.ToString("N0");
            }

            UtilityHook.FoxCore.MessagePlayerByUid(connection.Credentials.Uid, "nimbusfox.foxutils.teamfarming.status.name", team.Name);
            UtilityHook.FoxCore.MessagePlayerByUid(connection.Credentials.Uid, "nimbusfox.foxutils.teamfarming.status.petals", petals);
            UtilityHook.FoxCore.MessagePlayerByUid(connection.Credentials.Uid,
                team.SharePetals ? "nimbusfox.foxutils.teamfarming.status.sharepetalsenabled"
                    : "nimbusfox.foxutils.teamfarming.status.sharepetalsdisabled");
            UtilityHook.FoxCore.MessagePlayerByUid(connection.Credentials.Uid,
                team.AdminInviteOnly ? "nimbusfox.foxutils.teamfarming.status.ownerinvitesowner"
                    : "nimbusfox.foxutils.teamfarming.status.ownerinvitesall");
            UtilityHook.FoxCore.MessagePlayerByUid(connection.Credentials.Uid,
                "nimbusfox.foxutils.teamfarming.status.members", team.GetMemberCount().ToString("N0"));
            UtilityHook.FoxCore.MessagePlayerByUid(connection.Credentials.Uid,
                "nimbusfox.foxutils.teamfarming.status.owner",
                team.GetMembers().First(x => x.MemberUid == team.OwnerUid).DisplayName);

            foreach (var mem in team.GetMembers().Where(x => x.MemberUid != team.OwnerUid)) {
                UtilityHook.FoxCore.MessagePlayerByUid(connection.Credentials.Uid,
                    "nimbusfox.foxutils.teamfarming.status.member", mem.DisplayName);
            }

            return "";
        }

        private static string Invite(string[] bits, Entity callingEntity, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            responseParams = new object[] { };

            if (!UtilityHook.Instance.TeamFarmingHook.IsUserInTeam(callingEntity, out var member)) {
                return "nimbusfox.foxutils.teamfarming.error.notinteam";
            }

            var team = member.GetTeamRecord();

            if (team.AdminInviteOnly) {
                return "nimbusfox.foxutils.teamfarming.error.owneronly";
            }

            if (!bits.Any()) {
                UtilityHook.FoxCore.MessagePlayerByUid(connection.Credentials.Uid, "nimbusfox.foxutils.command.args");
                return "nimbusfox.foxutils.teamfarming.command.tfowner.help.invite";
            }

            var name = string.Join(" ", bits);
            var uid = UtilityHook.FoxCore.UserManager.GetUidByName(name);

            if (uid == null) {
                responseParams = new object[] { name };
                return "nimbusfox.foxutils.teamfarming.error.notonserver";
            }

            if (UtilityHook.Instance.TeamFarmingHook.IsUserInTeam(name, out _)) {
                responseParams = new object[] { name };
                return "nimbusfox.foxutils.teamfarming.error.currentlyinteam";
            }

            if (team.Banned.Contains(uid)) {
                responseParams = new object[] { name };
                return "nimbusfox.foxutils.teamfarming.error.inviteban";
            }

            if (team.GetMembers()
                .Any(x => string.Equals(x.DisplayName, name, StringComparison.CurrentCultureIgnoreCase))) {
                responseParams = new object[] { name };
                return "nimbusfox.foxutils.teamfarming.error.inviteinteam";
            }

            team.Invite(uid);

            foreach (var mem in team.GetMembers()) {
                UtilityHook.FoxCore.MessagePlayerByUid(mem.MemberUid, "nimbusfox.foxutils.teamfarming.success.newinvite", member.DisplayName, name);
            }

            UtilityHook.FoxCore.MessagePlayerByUid(uid, "nimbusfox.foxutils.teamfarming.message.invitetoteam", team.Name, member.DisplayName);

            return "";
        }
    }
}
