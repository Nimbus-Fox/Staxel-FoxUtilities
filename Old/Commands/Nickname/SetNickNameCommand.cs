﻿using NimbusFox.FoxUtilities.Hooks;
using Plukit.Base;
using Staxel.Commands;
using Staxel.Server;

namespace NimbusFox.FoxUtilities.Commands.Nickname {
    public class SetNickNameCommand : ICommandBuilder {
        public string Execute(string[] bits, Blob blob, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            responseParams = new object[] { };

            if (!UtilityHook.Instance.ServerConfig.FoxUtilsConfig.Enabled) {
                return "nimbusfox.foxutils.message.disabled";
            }

            if (!UtilityHook.Instance.ServerConfig.NickNamesConfig.Enabled) {
                return "nimbusfox.foxutils.nicknames.error.disabled";
            }
            
            var user = UtilityHook.FoxCore.UserManager.GetPlayerEntityByUid(connection.Credentials.Uid);

            if (bits.Length >= 3 && user.PlayerEntityLogic.IsAdmin()) {
                var player = UtilityHook.FoxCore.UserManager.GetPlayerEntityByName(bits[1]);

                UtilityHook.Instance.NickNameHook.SetNickName(player, bits[2]);

                UtilityHook.FoxCore.MessageAllPlayers("nimbusfox.foxutils.nicknames.newNickName",
                    UtilityHook.Instance.NickNameHook.GetOrigName(player), player.PlayerEntityLogic.DisplayName());

                return "";
            }

            if (bits.Length >= 2) {
                UtilityHook.Instance.NickNameHook.SetNickName(user, bits[1]);

                UtilityHook.FoxCore.MessageAllPlayers("nimbusfox.foxutils.nicknames.newNickName",
                    UtilityHook.Instance.NickNameHook.GetOrigName(user), user.PlayerEntityLogic.DisplayName());
            }

            return "";
        }

        public string Kind => "nick";
        public string Usage => "nimbusfox.foxutils.nicknames.command.nick.usage";

        public bool Public => !UtilityHook.Instance.ServerConfig.NickNamesConfig.AdminOnly &&
                              UtilityHook.Instance.ServerConfig.NickNamesConfig.Enabled &&
                              UtilityHook.Instance.ServerConfig.FoxUtilsConfig.Enabled;
    }
}
