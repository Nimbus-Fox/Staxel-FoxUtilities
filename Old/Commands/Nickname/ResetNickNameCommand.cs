﻿using NimbusFox.FoxUtilities.Hooks;
using Plukit.Base;
using Staxel.Commands;
using Staxel.Logic;
using Staxel.Server;

namespace NimbusFox.FoxUtilities.Commands.Nickname {
    public class ResetNickNameCommand : ICommandBuilder {
        public string Execute(string[] bits, Blob blob, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            responseParams = new object[] { };

            if (!UtilityHook.Instance.ServerConfig.FoxUtilsConfig.Enabled) {
                return "nimbusfox.foxutils.message.disabled";
            }

            if (!UtilityHook.Instance.ServerConfig.NickNamesConfig.Enabled) {
                return "nimbusfox.foxutils.nicknames.error.disabled";
            }

            Entity player;

            var user = UtilityHook.FoxCore.UserManager.GetPlayerEntityByUid(connection.Credentials.Uid);

            if (bits.Length >= 2 && user.PlayerEntityLogic.IsAdmin()) {
                UtilityHook.Instance.NickNameHook.RemoveNickName(bits[1]);

                player = UtilityHook.FoxCore.UserManager.GetPlayerEntityByName(bits[1]);

                if (player != default(Entity)) {
                    UtilityHook.Instance.NickNameHook.ResetNickName(player);
                    UtilityHook.FoxCore.MessageAllPlayers("nimbusfox.foxutils.nicknames.reset", UtilityHook.Instance.NickNameHook.GetOrigName(player));
                }

                responseParams = new object[] {bits[1]};

                return "nimbusfox.foxutils.nicknames.remove";
            }

            player = UtilityHook.FoxCore.UserManager.GetPlayerEntityByUid(connection.Credentials.Uid);

            UtilityHook.Instance.NickNameHook.ResetNickName(player);

            UtilityHook.FoxCore.MessageAllPlayers("nimbusfox.foxutils.nicknames.reset", UtilityHook.Instance.NickNameHook.GetOrigName(player));

            return "";
        }

        public string Kind => "resetnick";
        public string Usage => "nimbusfox.foxutils.nicknames.command.reset.usage";

        public bool Public => !UtilityHook.Instance.ServerConfig.NickNamesConfig.AdminOnly &&
                              UtilityHook.Instance.ServerConfig.NickNamesConfig.Enabled &&
                              UtilityHook.Instance.ServerConfig.FoxUtilsConfig.Enabled;
    }
}
