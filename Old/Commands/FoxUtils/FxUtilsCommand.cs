﻿using NimbusFox.FoxUtilities.Hooks;
using Plukit.Base;
using Staxel.Commands;
using Staxel.Logic;
using Staxel.Server;

namespace NimbusFox.FoxUtilities.Commands.FoxUtils {
    public class FxUtilsCommand : ICommandBuilder {
        public string Kind => "fxutils";
        public string Usage => "nimbusfox.foxutils.command.fxutils";
        public bool Public => true;

        public string Execute(string[] bits, Blob blob, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            responseParams = new object[] { };

            var callingEntity = UtilityHook.FoxCore.UserManager.GetPlayerEntityByUid(connection.Credentials.Uid);

            if (bits.Length > 1) {
                switch (bits[1].ToLower()) {
                    case "status":
                        return Status(bits, connection, api, out responseParams);
                    default:
                        if (callingEntity.PlayerEntityLogic.IsAdmin()) {
                            switch (bits[1].ToLower()) {
                                case "enable":
                                    return Enable(bits, callingEntity, connection, api, out responseParams);
                                case "disable":
                                    return Disable(bits, callingEntity, connection, api, out responseParams);
                                default:
                                    break;
                            }
                        }
                        break;
                }
            }

            return Help(bits, callingEntity, connection, api, out responseParams);
        }

        private string Help(string[] bits, Entity callingEntity, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            responseParams = new object[] { };

            UtilityHook.FoxCore.MessagePlayerByUid(connection.Credentials.Uid,
                "nimbusfox.foxutils.command.fxutils.help", Constants.Version.ToString());

            if (callingEntity.PlayerEntityLogic.IsAdmin()) {
                if (!UtilityHook.Instance.ServerConfig.FoxUtilsConfig.Enabled) {
                    return "nimbusfox.foxutils.command.fxutils.help.enable";
                }

                UtilityHook.FoxCore.MessagePlayerByUid(connection.Credentials.Uid,
                    "nimbusfox.foxutils.command.fxutils.help.disable");
            }

            UtilityHook.FoxCore.MessagePlayerByUid(connection.Credentials.Uid,
                "nimbusfox.foxutils.command.fxutils.help.status");
            

            return "";
        }

        private string Enable(string[] bits, Entity callingEntity, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            responseParams = new object[] { };

            if (UtilityHook.Instance.ServerConfig.FoxUtilsConfig.Enabled) {
                responseParams = new object[] { Constants.Version.ToString() };
                return "nimbusfox.foxutils.message.version";
            }

            UtilityHook.Instance.ServerConfig.FoxUtilsConfig.Enabled = true;
            UtilityHook.Instance.SettingsCheck();

            foreach (var entity in UtilityHook.FoxCore.UserManager.GetPlayerEntities()) {
                UtilityHook.Instance.OnPlayerConnect(entity);
            }

            return "";
        }

        private string Disable(string[] bits, Entity callingEntity, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            responseParams = new object[] { };

            if (!UtilityHook.Instance.ServerConfig.FoxUtilsConfig.Enabled) {
                return "nimbusfox.foxutils.message.disabled";
            }

            foreach (var entity in UtilityHook.FoxCore.UserManager.GetPlayerEntities()) {
                UtilityHook.Instance.OnPlayerDisconnect(entity);
            }

            UtilityHook.Instance.ServerConfig.FoxUtilsConfig.Enabled = false;

            UtilityHook.Instance.SettingsCheck();

            return "nimbusfox.foxutils.success.disabled";
        }

        private string Status(string[] bits, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            responseParams = new object[] { };

            if (!UtilityHook.Instance.ServerConfig.FoxUtilsConfig.Enabled) {
                return "nimbusfox.foxutils.message.disabled";
            }

            UtilityHook.FoxCore.MessagePlayerByUid(connection.Credentials.Uid, "nimbusfox.foxutils.status.backups",
                UtilityHook.Instance.ServerConfig.BackupConfig.Enabled ? Constants.Colors.Success : Constants.Colors.Error);

            UtilityHook.FoxCore.MessagePlayerByUid(connection.Credentials.Uid, "nimbusfox.foxutils.status.nicknames",
                UtilityHook.Instance.ServerConfig.NickNamesConfig.Enabled ? Constants.Colors.Success : Constants.Colors.Error);

            UtilityHook.FoxCore.MessagePlayerByUid(connection.Credentials.Uid,
                "nimbusfox.foxutils.status.reservedspace",
                UtilityHook.Instance.ServerConfig.ReservedSpaceConfig.Enabled ? Constants.Colors.Success : Constants.Colors.Error);

            UtilityHook.FoxCore.MessagePlayerByUid(connection.Credentials.Uid, "nimbusfox.foxutils.status.sleeptime",
                UtilityHook.Instance.ServerConfig.SleepTimeConfig.Enabled ? Constants.Colors.Success : Constants.Colors.Error);

            UtilityHook.FoxCore.MessagePlayerByUid(connection.Credentials.Uid, "nimbusfox.foxutils.status.teamfarming",
                UtilityHook.Instance.ServerConfig.TeamFarmingConfig.Enabled ? Constants.Colors.Success : Constants.Colors.Error);

            return "";
        }
    }
}
