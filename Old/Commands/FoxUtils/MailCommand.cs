﻿using NimbusFox.FoxUtilities.Hooks;
using Plukit.Base;
using Staxel;
using Staxel.Commands;
using Staxel.Server;

namespace NimbusFox.FoxUtilities.Commands.FoxUtils {
    public class MailCommand : ICommandBuilder {
        public string Execute(string[] bits, Blob blob, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            responseParams = new object[] { };

            if (!UtilityHook.Instance.ServerConfig.FoxUtilsConfig.AllowUserMail) {
                return "nimbusfox.foxutils.error.maildisabled";
            }

            if (bits.Length < 4) {
                UtilityHook.FoxCore.MessagePlayerByUid(connection.Credentials.Uid, "nimbusfox.foxutils.command.args");
                return "nimbusfox.foxutils.command.mail.help";
            }

            var bit = bits[1];
            var playerEntityId = api.FindPlayerEntityId(bit);
            var uid1 = (string)null;
            if (!playerEntityId.IsNull() && api.TryGetEntity(playerEntityId, out var entity))
                uid1 = entity.PlayerEntityLogic.Uid();
            if (uid1 == null && ServerContext.RightsManager.TryGetUIDByUsername(bit, out var uid2))
                uid1 = uid2;
            if (uid1 != null) {
                var mail = GameContext.MailDatabase.CreateMail(connection.Credentials.Username, bits[2], bits[3]);
                api.Facade().SendMail(uid1, mail, false);
                responseParams = new object[] { bit };
                return "commands.mail.success";
            }
            responseParams = new object[] { bit };
            return "commands.noSuchPlayer";
        }

        public string Kind => "fxmail";
        public string Usage => "nimbusfox.foxutils.command.mail";
        public bool Public => UtilityHook.Instance.ServerConfig.FoxUtilsConfig.AllowUserMail;
    }
}
