﻿using NimbusFox.FoxUtilities.Hooks;
using Plukit.Base;
using Staxel;
using Staxel.Commands;
using Staxel.Server;

namespace NimbusFox.FoxUtilities.Commands.FoxUtils {
    public class MailAllCommand : ICommandBuilder {
        public string Execute(string[] bits, Blob blob, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            responseParams = new object[] { };

            if (bits.Length < 3) {
                UtilityHook.FoxCore.MessagePlayerByUid(connection.Credentials.Uid, "nimbusfox.foxutils.command.args");
                return "nimbusfox.foxutils.command.mailall.help";
            }

            var mail = GameContext.MailDatabase.CreateMail(connection.Credentials.Username, bits[1], bits[2]);
            api.Facade().SendMailToAll(mail);
            return "nimbusfox.foxutils.success.mailsent";
        }

        public string Kind => "mailall";
        public string Usage => "nimbusfox.foxutils.command.mailall";
        public bool Public => false;
    }
}
