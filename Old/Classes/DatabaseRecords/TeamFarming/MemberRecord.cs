﻿using System;
using System.Linq;
using NimbusFox.FoxCore.V3.Classes;
using NimbusFox.FoxCore.V3.Classes.BlobRecord;
using NimbusFox.FoxCore.V3.Classes.FxBlob;
using NimbusFox.FoxUtilities.Hooks;

namespace NimbusFox.FoxUtilities.Classes.DatabaseRecords.TeamFarming {
    public class MemberRecord : BaseRecord {
        private readonly BlobDatabase _database;

        public MemberRecord(BlobDatabase database, FoxBlob blob, Guid id) : base(database, blob, id) {
            _database = database;
        }

        public string MemberUid {
            get => _blob.GetString(nameof(MemberUid), "");
            set {
                _blob.SetString(nameof(MemberUid), value);
                Save();
            }
        }

        public Guid TeamUid {
            get => _blob.GetGuid(nameof(TeamUid), Guid.Empty);
            set {
                _blob.SetGuid(nameof(TeamUid), value);
                Save();
            }
        }

        public long KickedAmount {
            get => _blob.GetLong(nameof(KickedAmount), 0);
            set {
                _blob.SetNumber(nameof(KickedAmount), value);
                Save();
            }
        }

        public long TeamPetalDiff {
            get => _blob.GetLong(nameof(TeamPetalDiff), 0);
            set {
                _blob.SetNumber(nameof(TeamPetalDiff), value);
                Save();
            }
        }

        public bool Imported {
            get => _blob.GetBool(nameof(Imported), true);
            set {
                _blob.SetBool(nameof(Imported), value);
                Save();
            }
        }

        public string DisplayName => UtilityHook.FoxCore.UserManager.GetNameByUid(MemberUid);

        public TeamRecord GetTeamRecord() {
            return _database.SearchRecords<TeamRecord>(x => x.ID == TeamUid).FirstOrDefault();
        }
    }
}
