﻿using System.IO;
using NimbusFox.FoxCore.V3.Classes;
using NimbusFox.FoxCore.V3.Classes.FxBlob;
using NimbusFox.FoxUtilities.Classes.BlobFiles.ConfigBlobs;

namespace NimbusFox.FoxUtilities.Classes.BlobFiles {
    internal class ConfigBlobFile : BlobFile {

        public NicknamesConfig NickNamesConfig { get; }
        public TeamFarmingConfig TeamFarmingConfig { get; }
        public SleepTimeConfig SleepTimeConfig { get; }
        public ScoreBoardConfig ScoreBoardConfig { get; }
        public ReservedSpaceConfig ReservedSpaceConfig { get; }
        public BackupConfig BackupConfig { get; }
        public FoxUtilsConfig FoxUtilsConfig { get; }

        public ConfigBlobFile(FileStream stream, bool binary = false) : base(stream, binary) {
            NickNamesConfig = new NicknamesConfig(this, Blob.FetchBlob("nicknamesConfig"));
            TeamFarmingConfig = new TeamFarmingConfig(this, Blob.FetchBlob("teamFarmingConfig"));
            SleepTimeConfig = new SleepTimeConfig(this, Blob.FetchBlob("sleepTimeConfig"));
            ScoreBoardConfig = new ScoreBoardConfig(this, Blob.FetchBlob("scoreBoardConfig"));
            ReservedSpaceConfig = new ReservedSpaceConfig(this, Blob.FetchBlob("reservedSpaceConfig"));
            BackupConfig = new BackupConfig(this, Blob.FetchBlob("backupConfig"));
            FoxUtilsConfig = new FoxUtilsConfig(this, Blob.FetchBlob("_foxUtilsConfig"));

            Defaults();
        }

        public ConfigBlobFile(FoxBlob blob) : base(null) {
            Blob.MergeFrom(blob);
            NickNamesConfig = new NicknamesConfig(this, Blob.FetchBlob("nicknamesConfig"));
            TeamFarmingConfig = new TeamFarmingConfig(this, Blob.FetchBlob("teamFarmingConfig"));
            SleepTimeConfig = new SleepTimeConfig(this, Blob.FetchBlob("sleepTimeConfig"));
            ScoreBoardConfig = new ScoreBoardConfig(this, Blob.FetchBlob("scoreBoardConfig"));
            ReservedSpaceConfig = new ReservedSpaceConfig(this, Blob.FetchBlob("reservedSpaceConfig"));
            BackupConfig = new BackupConfig(this, Blob.FetchBlob("backupConfig"));
            FoxUtilsConfig = new FoxUtilsConfig(this, Blob.FetchBlob("_foxUtilsConfig"));
        }

        internal void InternalSave() => Save();

        protected void Defaults() {
            NickNamesConfig.Defaults();
            TeamFarmingConfig.Defaults();
            SleepTimeConfig.Defaults();
            ScoreBoardConfig.Defaults();
            ReservedSpaceConfig.Defaults();
            BackupConfig.Defaults();
            FoxUtilsConfig.Defaults();

            ForceSave();
        }
    }
}
