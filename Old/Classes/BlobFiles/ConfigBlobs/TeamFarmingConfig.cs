﻿using NimbusFox.FoxCore.V3.Classes.FxBlob;

namespace NimbusFox.FoxUtilities.Classes.BlobFiles.ConfigBlobs {
    internal class TeamFarmingConfig : BaseConfig {
        public TeamFarmingConfig(ConfigBlobFile file, FoxBlob blob) : base(file, blob) { }

        public bool Enabled {
            get => Blob.GetBool("_enabled", true);
            set {
                Blob.SetBool("_enabled", value);
                File.InternalSave();
            }
        }

        public bool LimitMemberSize {
            get => Blob.GetBool("_limitMemberSize", true);
            set {
                Blob.SetBool("_limitMemberSize", value);
                File.InternalSave();
            }
        }

        public long CostToFormTeam {
            get => Blob.GetLong("costToFormTeam", 1000);
            set {
                Blob.SetNumber("costToFormTeam", value);
                File.InternalSave();
            }
        }

        public long MaxMembers {
            get => Blob.GetLong("maxMembers", 10);
            set {
                Blob.SetNumber("maxMembers", value);
                File.InternalSave();
            }
        }

        internal override void Defaults() {
            Enabled = Enabled;
            LimitMemberSize = LimitMemberSize;
            CostToFormTeam = CostToFormTeam;
            MaxMembers = MaxMembers;
        }
    }
}
