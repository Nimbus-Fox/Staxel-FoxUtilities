﻿using System.Collections.Generic;
using System.Linq;
using NimbusFox.FoxCore.V3.Classes.FxBlob;

namespace NimbusFox.FoxUtilities.Classes.BlobFiles.ConfigBlobs {
    internal class ScoreBoardConfig : BaseConfig {
        public ScoreBoardConfig(ConfigBlobFile file, FoxBlob blob) : base(file, blob) { }

        public bool Enabled {
            get => Blob.GetBool("_enabled", false);
            set {
                Blob.SetBool("_enabled", value);
                File.InternalSave();
            }
        }

        public List<string> Track {
            get {
                if (Blob.Contains("track")) {
                    return Blob.GetList<string>("track").ToList();
                }

                return new List<string>();
            }
            set {
                Blob.SetList("track", value);
                File.InternalSave();
            }
        }

        internal override void Defaults() {
            Enabled = Enabled;
            Track = Track;
        }
    }
}
