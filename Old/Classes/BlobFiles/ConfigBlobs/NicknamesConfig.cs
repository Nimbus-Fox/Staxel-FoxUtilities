﻿using NimbusFox.FoxCore.V3.Classes.FxBlob;

namespace NimbusFox.FoxUtilities.Classes.BlobFiles.ConfigBlobs {
    internal class NicknamesConfig : BaseConfig {

        public NicknamesConfig(ConfigBlobFile file, FoxBlob blob) : base(file, blob) { }

        public bool AdminOnly {
            get => Blob.GetBool("adminOnly", true);
            set {
                Blob.SetBool("adminOnly", value);
                File.InternalSave();
            }
        }

        public bool AllowColor {
            get => Blob.GetBool("allowColor", false);
            set {
                Blob.SetBool("allowColor", value);
                File.InternalSave();
            }
        }

        public bool Enabled {
            get => Blob.GetBool("_enabled", true);
            set {
                Blob.SetBool("_enabled", value);
                File.InternalSave();
            }
        }

        internal override void Defaults() {
            AdminOnly = AdminOnly;
            AllowColor = AllowColor;
            Enabled = Enabled;
        }
    }
}
