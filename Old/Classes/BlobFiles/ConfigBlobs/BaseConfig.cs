﻿using NimbusFox.FoxCore.V3.Classes.FxBlob;

namespace NimbusFox.FoxUtilities.Classes.BlobFiles.ConfigBlobs {
    internal abstract class BaseConfig {
        protected FoxBlob Blob;
        protected ConfigBlobFile File;

        public BaseConfig(ConfigBlobFile file, FoxBlob blob) {
            File = file;
            Blob = blob;
        }

        internal abstract void Defaults();
    }
}
