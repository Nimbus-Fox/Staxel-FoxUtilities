﻿namespace NimbusFox.FoxUtilities.Classes {
    public class Version {
        public byte Major { get; }
        public byte Minor { get; }

        public Version(byte major, byte minor) {
            Major = major;
            Minor = minor;
        }

        public override string ToString() {
            return $"{Major}.{Minor}";
        }

        public bool IsCompatible(Version version) {
            if (version.Major < Major) {
                return false;
            }

            if (version.Major == Major) {
                if (version.Minor < Minor) {
                    return false;
                }
            }

            return true;
        }
    }
}
