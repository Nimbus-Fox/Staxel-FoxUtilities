﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using NimbusFox.FoxCore.V3.Classes.FxBlob;
using NimbusFox.FoxCore.V3.Interfaces;
using NimbusFox.FoxUtilities.Classes.BlobFiles;
using NimbusFox.FoxUtilities.Patches;
using Plukit.Base;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Player;
using Staxel.Sky;
using Staxel.Tiles;
using Extensions = NimbusFox.FoxCore.V3.Extensions;

namespace NimbusFox.FoxUtilities.Hooks {
    internal class UtilityHook : IFoxModHook {

        static UtilityHook() {
            FoxCore = new FoxCore.V3.FoxCore("NimbusFox", "Fox Utils", Constants.Version.ToString());
        }

        internal readonly Dictionary<EntityId, DateTime> KickWaits = new Dictionary<EntityId, DateTime>();

        internal static UtilityHook Instance;

        internal static readonly FoxCore.V3.FoxCore FoxCore;

        internal ConfigBlobFile ServerConfig;

        internal NickNameHook NickNameHook;
        internal TeamFarmingHook TeamFarmingHook;
        internal SleepTimeHook SleepTimeHook;
        internal ReservedSpaceHook ReservedSpaceHook;
        private bool _needStore;

        internal void NeedsStore() {
            _needStore = true;
        }

        private DateTime _lastRun = DateTime.MinValue;

        public UtilityHook() {
            Instance = this;

            if (Process.GetCurrentProcess().ProcessName.StartsWith("Staxel.Server")) {
                ServerConfig = new ConfigBlobFile(FoxCore.ConfigDirectory.ObtainFileStream("foxUtils_Server.json", FileMode.OpenOrCreate, FileAccess.ReadWrite));

                SettingsCheck();
            }

            ServerMainLoopPatches.InitPatches();

            ChatControllerPatches.InitPatches();
        }

        public void SettingsCheck(bool server = true) {
            if (!ServerConfig.FoxUtilsConfig.Enabled) {
                NickNameHook?.Dispose();
                NickNameHook = null;
                TeamFarmingHook?.Dispose();
                TeamFarmingHook = null;
                ReservedSpaceHook?.Dispose();
                ReservedSpaceHook = null;
                SleepTimeHook?.Dispose();
                SleepTimeHook = null;

                Extensions.SetPrivatePropertyValue(null, "SundownPhase", 0.9f, typeof(DayNightCycle));
                Extensions.SetPrivatePropertyValue(null, "DawnPhase", 0.15f, typeof(DayNightCycle));
                Extensions.SetPrivatePropertyValue(null, "MinsPerDay", 1440, typeof(DayNightCycle));

                if (server) {
                    Logger.WriteLine("[Fox Utils] Fox Utils is disabled. To enable. Use /fxutils enable");
                } else {
                    Logger.WriteLine("[Fox Utils] Fox Utils is disabled on this server");
                }

                NeedsStore();
                return;
            }

            if (server) {
                if (ServerConfig.NickNamesConfig.Enabled) {
                    if (NickNameHook == null) {
                        NickNameHook = new NickNameHook();
                    }
                    Logger.WriteLine("[Fox Utils] Nicknames Enabled");
                } else {
                    NickNameHook?.Dispose();
                    NickNameHook = null;
                    Logger.WriteLine("[Fox Utils] Nicknames Disabled");
                }

                if (ServerConfig.TeamFarmingConfig.Enabled) {
                    if (TeamFarmingHook == null) {
                        TeamFarmingHook = new TeamFarmingHook();
                        Logger.WriteLine("[Fox Utils] TeamFarming Enabled");
                    }
                } else {
                    TeamFarmingHook?.Dispose();
                    TeamFarmingHook = null;
                    Logger.WriteLine("[Fox Utils] TeamFarming Disabled");
                }

                if (ServerConfig.ReservedSpaceConfig.Enabled) {
                    if (ReservedSpaceHook == null) {
                        ReservedSpaceHook = new ReservedSpaceHook();
                        Logger.WriteLine("[Fox Utils] ReservedSpaces Enabled");
                    }
                } else {
                    ReservedSpaceHook?.Dispose();
                    ReservedSpaceHook = null;
                    Logger.WriteLine("[Fox Utils] ReservedSpaces Disabled");
                }
            }

            if (ServerConfig.SleepTimeConfig.Enabled) {
                if (SleepTimeHook == null) {
                    SleepTimeHook = new SleepTimeHook();
                    Logger.WriteLine("[Fox Utils] SleepTime Enabled");
                }
                SleepTimeHook.ResetLastSeason();
            } else {
                SleepTimeHook?.Dispose();
                SleepTimeHook = null;
                Logger.WriteLine("[Fox Utils] SleepTime Disabled");

                Extensions.SetPrivatePropertyValue(null, "SundownPhase", 0.9f, typeof(DayNightCycle));
                Extensions.SetPrivatePropertyValue(null, "DawnPhase", 0.15f, typeof(DayNightCycle));
                Extensions.SetPrivatePropertyValue(null, "MinsPerDay", 1440, typeof(DayNightCycle));
            }

            NeedsStore();
        }

        public void Dispose() {
            NickNameHook?.Dispose();
            TeamFarmingHook?.Dispose();
            SleepTimeHook?.Dispose();
            ReservedSpaceHook?.Dispose();
        }

        public void GameContextInitializeInit() { }
        public void GameContextInitializeBefore() { }
        public void GameContextInitializeAfter() { }
        public void GameContextDeinitialize() { }
        public void GameContextReloadBefore() { }
        public void GameContextReloadAfter() { }

        internal int GetSeasonTime(Universe universe) {
            if (ServerConfig?.SleepTimeConfig == null || SleepTimeHook == null) {
                return 24 * 60;
            }

            var lengths = ServerConfig.SleepTimeConfig.DayLength;
            switch (universe.DayNightCycle().GetSeason()) {
                default:
                case 0:
                    return lengths.Spring * 60;
                case 1:
                    return lengths.Summer * 60;
                case 2:
                    return lengths.Autumn * 60;
                case 3:
                    return lengths.Winter * 60;
            }
        }

        public void UniverseUpdateBefore(Universe universe, Timestep step) {
            if (!universe.Server) {


            } else {
                if ((DateTime.Now - _lastRun).TotalSeconds >= 1 && ServerConfig.FoxUtilsConfig.Enabled) {
                    foreach (var kickWait in new Dictionary<EntityId, DateTime>(KickWaits)) {
                        if ((kickWait.Value - DateTime.Now).TotalSeconds <= 0) {
                            var player = FoxCore.UserManager.GetPlayerEntities().FirstOrDefault(x => x.Id == kickWait.Key);

                            if (player == default(Entity)) {
                                continue;
                            }

                            var connection = FoxCore.ServerMainLoop.FetchConnectionsByUid(player.PlayerEntityLogic.Uid());

                            foreach (var con in connection) {
                                con.ErrorAndClose("You do not have Fox Utils installed and such cannot play on this server");
                            }

                            FoxCore.MessageAllPlayers("nimbusfox.foxutils.message.kicked", player.PlayerEntityLogic.DisplayName());

                            KickWaits.Remove(kickWait.Key);
                        }
                    }

                    _lastRun = DateTime.Now;
                }
            }

            TeamFarmingHook?.UniverseUpdateBefore(universe, step);
            SleepTimeHook?.UniverseUpdateBefore(universe, step);
        }
        public void UniverseUpdateAfter() { }
        public bool CanPlaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            return true;
        }

        public bool CanReplaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            return true;
        }

        public bool CanRemoveTile(Entity entity, Vector3I location, TileAccessFlags accessFlags) {
            return true;
        }

        public void ClientContextInitializeInit() { }
        public void ClientContextInitializeBefore() { }
        public void ClientContextInitializeAfter() { }
        public void ClientContextDeinitialize() { }
        public void ClientContextReloadBefore() { }
        public void ClientContextReloadAfter() { }
        public void CleanupOldSession() { }

        public bool CanInteractWithTile(Entity entity, Vector3F location, Tile tile) {
            return true;
        }

        public bool CanInteractWithEntity(Entity entity, Entity lookingAtEntity) {
            return true;
        }

        public void OnPlayerLoadAfter(Blob blob) { }
        public void OnPlayerSaveBefore(PlayerEntityLogic logic, out Blob saveBlob) {
            saveBlob = null;
        }

        public void OnPlayerSaveAfter(PlayerEntityLogic logic, out Blob saveBlob) {
            saveBlob = null;
        }

        public void OnPlayerConnect(Entity entity) {
            if (ServerConfig.FoxUtilsConfig.Enabled) {
                if (ReservedSpaceHook != null) {
                    if (!ReservedSpaceHook.OnPlayerConnect(entity)) {
                        return;
                    }
                }

                FoxCore.MessagePlayerByEntity(entity, "Foxutils-Hello-Client");
                KickWaits.Add(entity.Id, DateTime.Now.AddSeconds(2));

                NickNameHook?.OnPlayerConnect(entity);
                TeamFarmingHook?.OnPlayerConnect(entity);
            }
            NeedsStore();
        }

        public void OnPlayerDisconnect(Entity entity) {
            if (ServerConfig.FoxUtilsConfig.Enabled) {
                TeamFarmingHook?.OnPlayerDisconnect(entity);
            }
        }
        
        // ReSharper disable UseNullPropagation
        public void Store(Blob blob) {
            if (_needStore) {
                blob.FetchBlob("settings").AssignFrom(ServerConfig.ToStaxelBlob());
                _needStore = false;
            }
        }
        // ReSharper enable UseNullPropagation

        public void Restore(Blob blob) {
            if (blob.Contains("settings")) {
                if (ServerConfig == null) {
                    ServerConfig = new ConfigBlobFile(FoxBlob.FromJson(blob.FetchBlob("settings").ToString()));
                } else {
                    ServerConfig.Load(blob.FetchBlob("settings"));
                }

                Logger.WriteLine("[Fox Utils] Got config from server");

                SettingsCheck(false);
            }
        }
    }
}
