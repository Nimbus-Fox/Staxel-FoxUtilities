﻿using System;
using System.Collections.Generic;
using System.Linq;
using Plukit.Base;
using Staxel.Logic;
using Staxel.Sky;
using Extensions = NimbusFox.FoxCore.V3.Extensions;

namespace NimbusFox.FoxUtilities.Hooks {
    internal class SleepTimeHook : IDisposable {

        private List<Entity> _sleeping = new List<Entity>();
        private List<Entity> _toRemove = new List<Entity>();

        private int _lastSeason = 4;

        private bool _needStore = false;
        private bool _canRun = false;

        public void Dispose() {
            _sleeping.Clear();
        }

        public void UniverseUpdateBefore(Universe universe, Timestep step) {
            if (_lastSeason != universe.DayNightCycle().GetSeason()) {
                _lastSeason = universe.DayNightCycle().GetSeason();

                Extensions.SetPrivatePropertyValue(null, "SundownPhase", UtilityHook.Instance.ServerConfig.SleepTimeConfig.SleepPhase, typeof(DayNightCycle));
                Extensions.SetPrivatePropertyValue(null, "DawnPhase", UtilityHook.Instance.ServerConfig.SleepTimeConfig.WakeUpPhase, typeof(DayNightCycle));
                Extensions.SetPrivatePropertyValue(null, "MinsPerDay", UtilityHook.Instance.GetSeasonTime(universe), typeof(DayNightCycle));

                Logger.WriteLine($"[Fox Utils][Sleep Time] Day length has been changed to {UtilityHook.Instance.GetSeasonTime(universe) / 60} minutes");
            }

            if (_sleeping.Count > 0 && universe.DayNightCycle().Phase <= DayNightCycle.DawnPhase + 0.2f && _canRun) {
                _sleeping.Clear();
                _canRun = false;
            }

            if (!_canRun && universe.DayNightCycle().Phase >= DayNightCycle.SundownPhase - 0.2f) {
                _canRun = true;
            }

            if (universe.DayNightCycle().Phase >= DayNightCycle.SundownPhase) {
                var sleeping = UtilityHook.FoxCore.UserManager.GetPlayerEntities()
                    .Where(x => x.PlayerEntityLogic.IsSleeping());
                var total = UtilityHook.FoxCore.UserManager.GetPlayerEntities().Count;
                var percentage = ((double)sleeping.Count() / total) * 100;

                if (double.IsInfinity(percentage)) {
                    percentage = 0;
                }

                foreach (var sleep in sleeping) {
                    if (!_sleeping.Contains(sleep)) {
                        _sleeping.Add(sleep);
                        if (UtilityHook.Instance.ServerConfig.SleepTimeConfig.Broadcast) {
                            UtilityHook.FoxCore.MessageAllPlayers("nimbusfox.foxutils.sleeptime.sleeping",
                                sleep.PlayerEntityLogic.DisplayName(), sleeping.Count(), total, percentage);
                        }
                    }
                }

                foreach (var awake in _sleeping) {
                    if (!awake.PlayerEntityLogic.IsSleeping() && UtilityHook.Instance.ServerConfig.SleepTimeConfig.Broadcast) {
                        UtilityHook.FoxCore.MessageAllPlayers("nimbusfox.foxutils.sleeptime.awake",
                            awake.PlayerEntityLogic.DisplayName(), sleeping.Count(), total, percentage);
                        _toRemove.Add(awake);
                    }
                }

                foreach (var awake in _toRemove) {
                    _sleeping.Remove(awake);
                }

                _toRemove.Clear();

                if (percentage >= UtilityHook.Instance.ServerConfig.SleepTimeConfig.PercentageToForceSleep) {
                    universe.AdvanceTime(0.8 - universe.DayNightCycle().Phase);
                }
            }
        }

        internal void ResetLastSeason() {
            _lastSeason = 4;
        }
    }
}
