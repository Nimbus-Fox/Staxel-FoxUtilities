﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using NimbusFox.FoxCore.V3.Classes;
using NimbusFox.FoxUtilities.Classes.DatabaseRecords.TeamFarming;
using Plukit.Base;
using Staxel;
using Staxel.Logic;
using Staxel.Sky;

namespace NimbusFox.FoxUtilities.Hooks {
    internal class TeamFarmingHook : IDisposable {
        private const string DatabaseFileName = "TeamFarming.db";

        public void Dispose() {
            _database.Dispose();
        }

        private readonly BlobDatabase _database;
        private readonly List<TeamRecord> _teams = new List<TeamRecord>();
        private bool _runMail;

        internal TeamFarmingHook() {
            _database = new BlobDatabase(
                UtilityHook.FoxCore.SaveDirectory.FetchDirectory("TeamFarming")
                    .ObtainFileStream(DatabaseFileName, FileMode.OpenOrCreate), UtilityHook.FoxCore.LogError);
        }

        public void UniverseUpdateBefore(Universe universe, Timestep step) {
            var players = UtilityHook.FoxCore.UserManager.GetPlayerEntities();

            _teams.Clear();

            foreach (var player in players) {
                if (IsUserInTeam(player, out var member)) {
                    var team = member.GetTeamRecord();
                    if (team.SharePetals) {
                        team.PetalDiff += player.Inventory.GetMoney() - team.Petals;
                        member.TeamPetalDiff += player.Inventory.GetMoney() - team.Petals;

                        if (!_teams.Any(x => x.ID == team.ID) && team.PetalDiff != 0) {
                            _teams.Add(team);
                        }
                    }
                }
            }

            foreach (var team in _teams) {
                if (team.PetalDiff != 0) {
                    team.Petals += team.PetalDiff;

                    foreach (var member in team.GetMembers()) {
                        if (UtilityHook.FoxCore.UserManager.IsUserOnline(member.MemberUid)) {
                            var current = UtilityHook.FoxCore.UserManager.GetPlayerEntityByUid(member.MemberUid);

                            current.Inventory.ResetMoney((int)team.Petals);
                        }
                    }

                    team.PetalDiff = 0;
                }
            }

            if (_runMail) {
                if (universe.DayNightCycle().Phase <= DayNightCycle.DawnPhase + 0.2f) {
                    foreach (var team in _teams) {
                        var message = new StringBuilder();

                        message.Append("Hello {0},\\n\\n");

                        var members = team.GetMembers();

                        var total = 0L;

                        message.Append("Member report:\\n\\n");

                        members.SortBy(x => x.TeamPetalDiff);

                        foreach (var member in members) {
                            total += member.TeamPetalDiff;

                            message.Append(
                                $"{member.DisplayName}: {(member.TeamPetalDiff < 0 ? Constants.Colors.Error : Constants.Colors.Success)}{member.TeamPetalDiff}^c:pop; Petals\\n");
                        }

                        message.Append("\\n");

                        message.Append("Total: " + total + " Petals\\n\\n");

                        message.Append("-Fox Utils Team Farming");

                        foreach (var member in members) {
                            var mail = GameContext.MailDatabase.CreateMail("Fox Utils", "Team Farming Report", string.Format(message.ToString(), member.DisplayName));
                            ServerContext.VillageDirector.UniverseFacade.SendMail(member.MemberUid, mail, true);
                        }
                    }

                    _runMail = false;
                }
            }

            if (!_runMail) {
                if (universe.DayNightCycle().Phase >= DayNightCycle.SundownPhase - 0.2f) {
                    _runMail = true;
                }
            }
        }

        public void OnPlayerConnect(Entity entity) {
            if (IsUserInTeam(entity, out var member)) {
                var team = member.GetTeamRecord();
                if (team.SharePetals) {
                    if (!member.Imported) {
                        member.TeamPetalDiff += entity.Inventory.GetMoney();
                        team.PetalDiff += entity.Inventory.GetMoney();
                    }
                    entity.Inventory.ResetMoney((int)member.GetTeamRecord().Petals);
                }
            }

            if (member != null) {
                if (member.KickedAmount > 0) {
                    entity.Inventory.ResetMoney((int)member.KickedAmount);
                    member.KickedAmount = 0;
                }
            }
        }

        public void OnPlayerDisconnect(Entity entity) {
            if (IsUserInTeam(entity, out var member)) {
                if (member.GetTeamRecord().SharePetals) {
                    entity.Inventory.ResetMoney((int)member.GetTeamRecord().GetDisbandMoney());
                }
            }
        }

        public bool IsUserInTeam(Entity entity, out MemberRecord member) {
            if (entity?.PlayerEntityLogic != null) {
                var record = _database.SearchRecords<MemberRecord>(x => x.MemberUid == entity.PlayerEntityLogic.Uid())
                    .FirstOrDefault();

                if (record != default(MemberRecord)) {
                    member = record;
                    return record.GetTeamRecord() != null;
                }
            }

            member = null;
            return false;
        }

        public bool IsUserInTeam(string nameUid, out MemberRecord member) {
            var record = _database.SearchRecords<MemberRecord>(x =>
                string.Equals(x.MemberUid, nameUid, StringComparison.CurrentCultureIgnoreCase) ||
                string.Equals(x.DisplayName, nameUid, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();

            if (record != default(MemberRecord)) {
                member = record;
                return record.GetTeamRecord() != null;
            }

            member = null;
            return false;
        }

        public TeamRecord CreateTeam(string name, bool sharePetals, bool adminInviteOnly) {
            if (TeamExists(name)) {
                return null;
            }

            var record = _database.CreateRecord<TeamRecord>();

            record.Name = name;
            record.AdminInviteOnly = adminInviteOnly;
            record.SharePetals = sharePetals;

            return record;
        }

        public bool TeamExists(string name) {
            return _database
                .SearchRecords<TeamRecord>(x => string.Equals(x.Name, name, StringComparison.CurrentCultureIgnoreCase) && !x.Disbanded)
                .Any();
        }

        public TeamRecord GetTeam(string name) {
            if (!TeamExists(name)) {
                return default(TeamRecord);
            }

            return _database.SearchRecords<TeamRecord>(x =>
                    string.Equals(x.Name, name, StringComparison.CurrentCultureIgnoreCase) && !x.Disbanded)
                .FirstOrDefault();
        }
    }
}
