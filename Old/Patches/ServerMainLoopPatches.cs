﻿using NimbusFox.FoxCore.V3;
using NimbusFox.FoxUtilities.Hooks;
using Plukit.Base;
using Staxel.Server;

namespace NimbusFox.FoxUtilities.Patches {
    internal static class ServerMainLoopPatches {
        public static void InitPatches() {
            UtilityHook.FoxCore.PatchController.Add(typeof(ServerMainLoop), "HandleConsoleMessage",
                typeof(ServerMainLoopPatches), nameof(ConsoleMessage));
        }

        private static bool ConsoleMessage(Blob blob, ClientServerConnection connection) {
            if (!blob.Contains("message")) {
                return true;
            }

            try {
                var message = blob.GetString("message");
                var textBlob = BlobAllocator.Blob(true);

                textBlob.ReadJson(message);

                if (textBlob.Contains("Foxutils-Hello-Server")) {
                    var version = textBlob.GetObject<Classes.Version>("Foxutils-Hello-Server");

                    if (Constants.Version.IsCompatible(version)) {
                        UtilityHook.Instance.KickWaits.Remove(connection.ConnectionEntityId);

                        UtilityHook.FoxCore.MessagePlayerByUid(connection.Credentials.Uid, "nimbusfox.foxutils.message.version", Constants.Version.ToString());
                    } else {
                        connection.ErrorAndClose($"Please update your Fox Utils. Server Version: {Constants.Version}. Client Version: {version}");
                        Logger.WriteLine("Client was disconnected for having an outdated Fox Utils");
                    }

                    Blob.Deallocate(ref textBlob);
                    return false;
                }

                Blob.Deallocate(ref textBlob);
            } catch {
                //ignore
            }

            return true;
        }
    }
}
