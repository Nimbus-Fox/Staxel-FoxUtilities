﻿using System.Collections.Generic;
using NimbusFox.FoxCore.Dependencies.Harmony;
using NimbusFox.FoxCore.V3;
using NimbusFox.FoxUtilities.Hooks;
using Plukit.Base;
using Staxel;
using Staxel.Client;

namespace NimbusFox.FoxUtilities.Patches {
    internal static class ChatControllerPatches {

        public static readonly List<(string search, string replace)> Replacements = new List<(string search, string replace)>();

        public static void InitPatches() {
            UtilityHook.FoxCore.PatchController.Add(typeof(ChatController), "ReceiveConsoleResponse",
                typeof(ChatControllerPatches), nameof(ReceiveConsoleResponse));

            Replacements.Add(("##WHITE##", "^c:FFFFFF;"));
            Replacements.Add(("##ERROR##", Constants.Colors.Error));
            Replacements.Add(("##COMMAND##", Constants.Colors.Command));
            Replacements.Add(("##SUBCOMMAND##", Constants.Colors.SubCommand));
            Replacements.Add(("##SUBCOMMANDSEPERATOR##", Constants.Colors.SubCommandSeparator));
            Replacements.Add(("##REQUIREDARGUMENT##", Constants.Colors.RequiredArgument));
            Replacements.Add(("##OPTIONALARGUMENT##", Constants.Colors.OptionalArgument));
            Replacements.Add(("##FOXUTILS##", Constants.Colors.FoxUtils));
            Replacements.Add(("##SUCCESS##", Constants.Colors.Success));
            Replacements.Add(("##INFO##", Constants.Colors.Info));
            Replacements.Add(("##PETALS##", Constants.Colors.Petals));
        }

        public static bool ReceiveConsoleResponse(ChatController __instance, Blob blob) {
            if (blob.Contains("response")) {
                var str1 = blob.GetString("response");

                if (str1 == "Foxutils-Hello-Client") {
                    var textBlob = BlobAllocator.Blob(true);
                    textBlob.SetObject("Foxutils-Hello-Server", Constants.Version);

                    AccessTools.Method(__instance.GetType(), "OnInput")
                        .Invoke(__instance, new object[] { textBlob.ToString() });

                    Blob.Deallocate(ref textBlob);

                    return false;
                }

                var translated = ClientContext.LanguageDatabase.GetTranslationString(str1);
                var colored = false;

                foreach (var (search, replace) in Replacements) {
                    if (translated.Contains(search)) {
                        colored = true;
                        translated = translated.Replace(search, replace);
                    }
                }

                if (colored) {
                    blob.SetString("response", translated);
                }
                return true;
            }

            return true;
        }
    }
}
